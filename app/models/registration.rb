class Registration < ApplicationRecord
  belongs_to :event
  belongs_to :user

  validate :valid_date_range

  private

  def valid_date_range
    if starts_at.present? && ends_at.present? &&
        starts_at.to_date == ends_at.to_date &&
         ends_at > starts_at &&
          starts_at >= event.starts_at && ends_at <= event.ends_at &&
           !time_frame_overlap?

      return true
    end

    byebug
   
    errors.add(:time_end, 'Invalid date range')
  end

  def time_frame_overlap?
    event.registrations.find_each do |r|
      return true if starts_at < r.ends_at
    end
    false
  end
end 
