class User < ApplicationRecord
  has_many :registrations
  has_many :speaking_events, class_name: 'Event', foreign_key: 'speaker_id'

  validates :email, presence: true, uniqueness: true
end
