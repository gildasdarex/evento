class Event < ApplicationRecord
  has_one :speaker, class_name: 'User'
  has_many :registrations

  validates_presence_of :name, :description, :location, :speaker_id, :category, :starts_at, :ends_at, :max_participants
  validates_inclusion_of :category, in: %w(workshop office_hours)
  validates :max_participants, numericality: { only_integer: true }
  validates :speaker_id, numericality: { only_integer: true }
  validate :valid_date_range

  private

  def valid_date_range
    if starts_at.present? && ends_at.present? && starts_at.to_date == ends_at.to_date && ends_at > starts_at
      return true
    end
   
    errors.add(:starts_at, 'Invalid date range')
    errors.add(:ends_at, 'Invalid date range')
  end
 end