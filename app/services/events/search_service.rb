module Events
  class SearchService < Base
    attr_reader :params

    def initialize(params)
      @params = params
    end

    def call
      speaker_email = params.delete(:speaker_email)
      date          = params.delete(:date)
      
      @events = Event.where(params)

      filter_by_speaker(speaker_email) if speaker_email.present?
      filter_by_date(date) if date.present?

      @events
    end

    private

    def query_hash
      if params[:speaker_email]
        params[:speaker_id] = User.find_by(email: params.delete(:speaker_email))&.id
      end
    end

    def filter_by_speaker(speaker_email)
      speaker = User.where(email: speaker_email)
      
      if speaker.present?
        @events = @events.where(speaker_id: speaker.id)
      else
        @events = []
      end
    end

    def filter_by_date(date)
      @events = @events.where('starts_at::date = ?', params[:date])
    end
  end
end