module Events
  class CreateService < Base
    attr_reader :params

    def initialize(params)
      @params = params
    end

    def call
      event = nil
      registrations_data = params.delete(:registrations)
      Event.transaction do
        event= Event.create!(params)
  
        if event.category == 'office_hours' && registrations_data.present?
          event.registrations << build_registrations(registrations_data)
          event.save!
        end
      end

      event
    end

    private

    def build_registrations(registrations_data)
      registrations_data.map do |r|
        Registration.new(
          starts_at: r[:starts_at],
          ends_at: r[:ends_at], 
          user: User.find_by(email: r[:email])
        )
      end
    end
  end
end