class EventsController < ApplicationController
  before_action :check_speaker_presence, only: %i(create)
  before_action :set_event, only: %i(show)
  
  def index
  end

  def new
    @event = Event.new
  end

  def create
    params = event_params
    params.delete(:speaker_email)
    params.merge!(speaker_id: speaker.id)

    @event = Events::CreateService.call(params)

    if !@event.save
      flash[:error] = 'Could not create event'
      redirect_to new_event_path
    else
      flash[:success] = 'Successfully created event'
      redirect_to event_path(@event)
    end
  end

  def show
  end


  private

  def event_params
    params.require(:event).permit(
      :id,
      :name, 
      :description, 
      :location, 
      :starts_at, 
      :ends_at, 
      :category, 
      :speaker_email, 
      :max_participants, 
      registrations: [:email, :starts_at, :ends_at]
    )
  end

  def speaker
    @speaker ||= User.find_by(email: event_params[:speaker_email])
  end

  def check_speaker_presence
    return if speaker.present?

    flash[:error] = "Could not find any speaker with that email"
    redirect_to new_event_path
  end

  def set_event
    @event ||= Event.find(params[:id])
  end
end
