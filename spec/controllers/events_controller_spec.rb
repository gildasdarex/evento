require 'rails_helper'

RSpec.describe EventsController, type: :controller do
  let(:user1) { FactoryBot.create(:user, email: 'user1@example.com') }
  let(:user2) { FactoryBot.create(:user, email: 'user2@example.com') }

  let(:workshop_event_data) do
    { 
      event: { 
        name: 'Event1', 
        description: 'Some Event', 
        location: 'US, New York',
        starts_at: DateTime.now.beginning_of_day,
        ends_at: DateTime.now.beginning_of_day + 50.minutes,
        category: 'workshop',
        speaker_email: user1.email,
        max_participants: 10
      } 
    }
  end

  let(:office_hours_event_data) do
    {
      event: { 
        name: 'Event1', 
        description: 'Some Event', 
        location: 'US, New York',
        starts_at: DateTime.now.beginning_of_day,
        ends_at: DateTime.now.beginning_of_day + 50.minutes,
        category: 'office_hours',
        speaker_email: user1.email,
        max_participants: 10,
        registrations: [
          { email: user1.email, starts_at: DateTime.now.beginning_of_day, ends_at: DateTime.now.beginning_of_day + 10.minutes },
          { email: user2.email, starts_at: DateTime.now.beginning_of_day + 10.minutes, ends_at: DateTime.now.beginning_of_day + 20.minutes }
        ]
      }
    }
  end

  describe '.create' do
    it 'checks speaker presence' do
      expect { post :create, params: workshop_event_data.except(:speaker_email) }.not_to change { Event.count }
      expect(flash[:error]).to be_present
    end

    context 'workshop event' do
      it 'creates successfully' do
        expect { post :create, params: workshop_event_data }.to change { Event.count }.by(1)
        expect(flash[:success]).to be_present
      end
    end

    context 'office_hours event' do
      it 'creates successfully' do
        post :create, params: office_hours_event_data

        expect(Event.count).to eq(1)
        expect(Event.first.registrations.count).to eq(2)
      end
    end
  end
end
