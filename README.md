# README

## Clone the app
`git clone https://gitlab.com/gildasdarex/evento`

## Install the dependencies
`bundle install`

## Run the app
`bundle exec rails s`
